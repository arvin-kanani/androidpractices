package online.world.androidkotlinpractises.spotifyloginpage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_spotify_login_page_fragment.*
import online.world.androidkotlinpractises.R

class SpotifyLoginPageFragmentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spotify_login_page_fragment)

        setSupportActionBar(spotifyToolbar)
        supportActionBar?.title = "LoginPageWithFragment"
        supportActionBar?.title

    }
}