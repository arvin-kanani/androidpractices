package online.world.androidkotlinpractises.spotifyloginpage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.view.ViewCompat.setBackgroundTintList
import kotlinx.android.synthetic.main.activity_main.*
import online.world.androidkotlinpractises.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        changeBackground()
        changeActivity()

    }

    fun changeBackground() {
        etUsername.setOnTouchListener { view, motionEvent ->

            setBackgroundTintList(
                etUsername,
                getResources().getColorStateList(R.color.spotify_et_tint_color)
            )

            setBackgroundTintList(
                etPassword,
                getResources().getColorStateList(R.color.spotify_et_color)
            )

            return@setOnTouchListener false
        }

        etPassword.setOnTouchListener { view, motionEvent ->

            setBackgroundTintList(
                etPassword,
                getResources().getColorStateList(R.color.spotify_et_tint_color)
            )

            setBackgroundTintList(
                etUsername,
                getResources().getColorStateList(R.color.spotify_et_color)
            )


            return@setOnTouchListener false
        }

        val textWatcher1 = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

                if (etUsername.text.toString().isNotEmpty() && etPassword.text.toString()
                        .isNotEmpty()
                ) {
                    login_btn.background =
                        resources.getDrawable(R.drawable.spotify_login_btn_tint_background)
                    login_btn.isEnabled = true
                } else {
                    login_btn.background =
                        resources.getDrawable(R.drawable.spotify_login_btn_background)
                    login_btn.isEnabled = false
                }

            }

            override fun afterTextChanged(editable: Editable) {

            }
        }

        etUsername.addTextChangedListener(textWatcher1)
        etPassword.addTextChangedListener(textWatcher1)

    }

    fun changeActivity() {
        login_btn.setOnClickListener {
            val intent = Intent(this@MainActivity, SpotifyLoginPageFragmentActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}